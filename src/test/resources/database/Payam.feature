Feature: Validating Employees
  @db
  Scenario Outline:

    Given User connects to the database
    When User sends "<query>" to database and receives a list
    Then Validate first and last name of employees reporting to Payam
      | Neena   | Kochhar   |
      | Lex     | De Haan   |
      | Den     | Raphaely  |
      | Matthew | Weiss     |
      | Adam    | Fripp     |
      | Payam   | Kaufling  |
      | Shanta  | Vollman   |
      | Kevin   | Mourgos   |
      | John    | Russell   |
      | Karen   | Partners  |
      | Alberto | Errazuriz |
      | Gerald  | Cambrault |
      | Eleni   | Zlotkey   |
      | Michael | Hartstein |

    Examples: Database query
      | query                                                                                                                      |
      | SELECT first_name, last_name FROM employees WHERE manager_id = (SELECT manager_id FROM employees WHERE first_name='Payam') |
