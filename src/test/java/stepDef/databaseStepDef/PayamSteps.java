package stepDef.databaseStepDef;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import static utils.DBUtil.executeQuery;

public class PayamSteps {

    static String classQuery;
    static List<List<Object>> listOfListQuery = new ArrayList<>();

    @Given("User connects to the database")
    public void userConnectsToTheDatabase() {
        DBUtil.createDBConnection();
    }

    @When("User sends {string} to database and receives a list")
    public void userSendsToDatabaseAndReceivesAList(String query) {
        classQuery = query;
        executeQuery(classQuery);
        listOfListQuery = DBUtil.getQueryResultList(query);
    }

    @Then("Validate first and last name of employees reporting to Payam")
    public void validateFirstAndLastNameOfEmployeesReportingToPayam(DataTable dataTable) {
        Assert.assertEquals( listOfListQuery, dataTable.asLists());

    }
}
